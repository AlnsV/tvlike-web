import os
from flask import Flask, render_template, request


app = Flask(__name__)


@app.route('/', methods=["GET", "POST"])
def index():
    return render_template("index.html")


@app.route('/signin', methods=["GET", "POST"])
def sign():

    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']
        if len(username) > 3 and len(password) > 3:
            return render_template("account.hmtl")

    return render_template("signin.html")


if __name__ == '__main__':
    app.run()
