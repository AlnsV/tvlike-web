const tabItems = document.querySelectorAll('.tab-item');
const tabContentItems = document.querySelectorAll('.tab-content-item');

// Select tab content item
function selectItem(e) {
	// Remove all show and border classes
	removeBorder();
	removeShow();
	// Add border to current tab item
	this.classList.add('tab-border');
	// Grab content item from DOM
	const tabContentItem = document.querySelector(`#${this.id}-content`);
	// Add show class
	tabContentItem.classList.add('show');
}

// Remove bottom borders from all tab items
function removeBorder() {
	tabItems.forEach(item => {
		item.classList.remove('tab-border');
	});
}

// Remove show class from all content items
function removeShow() {
	tabContentItems.forEach(item => {
		item.classList.remove('show');
	});
}

// Listen for tab item click
tabItems.forEach(item => {
	item.addEventListener('click', selectItem);
});



const textInputs = document.querySelectorAll('.text-input');

const errors = {
  text: 'please enter a valid email',
  password: 'password must be between 4 and 60 characters long'
};

function toggleValidInput(e) {
  const { length } = e.target.value;
  const field = this.parentElement;
  const queryP = field.querySelector('p');

  if ((length < 4 || length > 60) && !queryP) {
    const p = document.createElement('p');
    const type = this.getAttribute('type');

    this.classList.add('invalid');
    p.classList.add('error-message');
    p.textContent = errors[type];
    field.appendChild(p);
  } else if (length >= 4 && (length <= 60 && queryP)) {
    this.classList.remove('invalid');
    queryP && queryP.remove();
  }
}

textInputs.forEach(input => {
  input.addEventListener('keyup', toggleValidInput);
});
