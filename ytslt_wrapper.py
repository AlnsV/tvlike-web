import requests
from loguru import logger


class YTSLTWrapper:
    def get_movies_list(self, limit: int = 20, page: int = 1, quality: str = "All",
                        minimum_rating: int = 0, query_term: str = "0", genre: str = "all",
                        sort_by: str = "date_added", order_by: str = "desc", with_rt_ratings: str = False):
        params = {
            "limit": limit,
            "page": page,
            "wuality": quality,
            "minimum_rating": minimum_rating,
            "query_term": query_term,
            "genre": genre,
            "sort_by": sort_by,
            "order_by": order_by,
            "with_rt_ratings": with_rt_ratings
        }
        data = requests.get(url="https://yts.lt/api/v2/list_movies.json",
                            params=params).json()["data"]["movies"]
        return data

    def get_movie_details(self, movie_id: int, with_images: bool = False, with_cast: bool = False):
        params = {
            "movie_id": movie_id,
            "with_images": with_images,
            "with_cast": with_cast,
        }
        data = requests.get(url="https://yts.lt/api/v2/movie_details.json",
                            params=params).json()["data"]["movie"]
        return data

    def get_movie_suggestions(self, movie_id: int):
        params = {
            "movie_id": movie_id
        }
        data = requests.get(url="https://yts.lt/api/v2/movie_suggestions.json",
                            params=params).json()["data"]["movies"]
        return data


if __name__ == "__main__":
    wrapper = YTSLTWrapper()
    movies = wrapper.get_movies_list(limit=20)
    logger.info(len(movies))
    movie_details = wrapper.get_movie_details(movie_id=1)
    logger.info(movie_details.keys())
    movie_suggestions = wrapper.get_movie_suggestions(movie_id=10)
    logger.info(len(movie_suggestions))
    movie_comments = wrapper.get_movie_comments(movie_id=49)
    logger.info(movie_comments)
